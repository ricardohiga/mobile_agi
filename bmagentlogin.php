#!/usr/bin/php
<?php

/**
 * Classe de entrada do BmConnector
 * Deve tratar as chamadas saintes e os hangups
 * para o bom funcionamento do sistema preditivo
 * 
 * @author Fabricio S Costa
 * @version 3.2.2
 * @since 2016/01/05
 */

/**
 * Imports
 */
require_once ('bmconnector/config/Bootstrap.php');
require_once ('bmconnector/tools/StringTools.php');
require_once ('phpagi/phpagi.php');

/**
 * Instanciando os objetos de classe
 */
$agi = new AGI();
$confs = new Bootstrap();

$url = '';

switch (strtolower($argv[1])) {
        case 'login':
                $url = sprintf('http://%s:3041/bmtelecom/predictive/agent/login/123456/%s/%s',
                                $confs->read('System.host'),
                                $argv[2], 
                                $argv[2]
                        );
        break;
        case 'logoff':
                $url = sprintf('http://%s:3041/bmtelecom/predictive/agent/logoff/123456/%s',
                                $confs->read('System.host'),
                                $argv[2]
                        );
        break;
}

$agi->noop('====> URL: ' .$url);

/**
 * Resposta do system via CUrl
 */
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
$output = trim(curl_exec($ch));
curl_close($ch);

exit ();
?>

