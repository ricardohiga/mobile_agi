#!/usr/bin/php

<?php

/**
 * Recebendo mensagem SMS pela placa
 */

$from = $argv[1];
$date = $argv[2].$argv[3];
/** $body = sanitizeString($argv[4]); **/
$body = $argv[4];
$status = $argv[5];

$url = "http://127.0.0.1/bmtelecom/connector/sms_received/$from/$body/$status";

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
$output = trim(curl_exec($ch));
curl_close($ch);

function sanitizeString($str) {
	$str = str_replace('/', '%99', $str);
	$str = preg_replace('/[áàãâä]/ui', 'a', $str);
	$str = preg_replace('/[éèêë]/ui', 'e', $str);
	$str = preg_replace('/[íìîï]/ui', 'i', $str);
	$str = preg_replace('/[óòõôö]/ui', 'o', $str);
	$str = preg_replace('/[úùûü]/ui', 'u', $str);
	$str = preg_replace('/[ç]/ui', 'c', $str);
	return $str;
}
?>

