#!/usr/bin/php
<?php

/**
 * Classe de Status Manual de 
 * 
 */

/**
 * Imports
 */
require_once ('bmconnector/config/Bootstrap.php');
require_once ('bmconnector/tools/StringTools.php');
require_once ('phpagi/phpagi.php');

/**
 * Instanciando os objetos de classe
 */
$agi = new AGI();
$confs = new Bootstrap();

/**
 * manual_status($phone_number = '', $status = '', $uniqueid = '',
 *           $agent = '', $linkedid = '',$queue='')
 */
$url = sprintf('http://%s/%s/connector/manual_status/%s/%s/%s/%s/%s/%s',
				$confs->read('System.host'),
				$confs->read('System.name'),
				$argv[1], 
				$argv[2],
				$argv[3],
				$argv[4],
				$argv[5],
				$argv[6]
			);

$agi->noop('====> URL: ' .$url);

/**
 * Resposta do system via CUrl
 */
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
$output = trim(curl_exec($ch));
curl_close($ch);

exit ();
?>
