#!/usr/bin/php
<?php

require_once('phpagi/phpagi.php');

$agi=new AGI();

$ramal=$argv[1];

/**
 * Ramais SIP do cliente
 * 
 */
$ramaisOneToOne = array (
                '7020',
                '7021',
                '7022',
                '7023',
                '7024',
                '7025',
                '7026',
                '7027',
                        );

$isOneToOne = in_array($ramal, $ramaisOneToOne) ? 'TRUE' : 'FALSE';
$agi->verbose("IsOneToOne: " . $isOneToOne);

$agi->set_variable("ISONETOONE",$isOneToOne);
exit();

?>