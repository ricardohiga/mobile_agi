#!/usr/bin/php
<?php

/**
 * Classe de entrada do BmConnector
 * Deve tratar as chamadas saintes e os hangups
 * para o bom funcionamento do sistema preditivo
 * 
 * @author Fabricio S Costa
 * @version 3.2.2
 * @since 2018/07/06
 */

/**
 * Imports
 */
require_once ('bmconnector/config/Bootstrap.php');
require_once ('bmconnector/tools/StringTools.php');
require_once ('phpagi/phpagi.php');

/**
 * Instanciando os objetos de classe
 */
$agi = new AGI();
$confs = new Bootstrap();

/**
 * 
 */
$url = sprintf('http://127.0.0.1:3001/bemmelhor/agent/hangup/123456/%s',
                $argv[1]
			);

$agi->noop('====> URL: ' .$url);

/**
 * Resposta do system via CUrl
 */
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
$output = trim(curl_exec($ch));
curl_close($ch);

exit ();
?>
