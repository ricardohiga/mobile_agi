#!/usr/bin/php
<?php

require_once('phpagi/phpagi.php');

$agi=new AGI();
$ramal=$argv[1];

/**
 * Operadoras validas para portabilidade
 * inserir novas de acordo com a necessidade
 */
$ramaisValidos = array (
        '', 
);

$response = in_array($ramal, $ramaisValidos) ? 'true' : 'false';

$agi->set_variable("CONTAINS2", $response);
exit();

?>

