#!/usr/bin/php
<?php

require_once ('phpagi/phpagi.php');
require_once ('bmconnector/tools/StringTools.php');

//$agi=new AGI();

/**
 * Recebendo mensagem SMS pela placa
 */

$host = 'www.bemmelhor.com.br/agendamentos'; 

$origin = $argv[1];
$agent = $argv[2];
$uniqueid = $argv[3];
$linkedid = $argv[4];

$url = sprintf("http://%s/rest_api/add_online_incoming/%s/%s/%s/%s",
        $host,
        StringTools::validateParam(StringTools::clean($origin)),
        StringTools::validateParam(StringTools::clean($agent)),
        StringTools::validateParam($uniqueid),
        StringTools::validateParam($linkedid));

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
$output = trim(curl_exec($ch));
curl_close($ch);

//$agi->noop('=> URL: ' .$url);
echo $url;
