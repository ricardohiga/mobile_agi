#!/usr/bin/php
<?php

/**
 * AGI para validacoes de ramais que poderao 
 * fazer ligacoes saintes
 * 
 * @author Fabricio S Costa fabriciojf@gmail.com
 * @version 1.0
 * @since 05/05/2016
 */
require_once('phpagi/phpagi.php');

$agi=new AGI();
$ramal=$argv[1];

/**
 * Edite o array $ramaisValidos adicionando ou removendo
 * ramais, somente os ramais contidos no 
 * array serao devolvidos com o	flag yes no parametro ${PERMITIR}
 * 
 * Dentro do contexto from-internal os ramais 
 * devem ter ser validados da seguinte forma
 * 
 * exten => _00ZX.,n,Gosubif($["${PERMITIR}"="yes"]?yesddi:noddi)
 * exten => _00ZX.,n(yesddi),NoOp(permitir) 
 * ...
 * exten => _00ZX.,n,Hangup()
 * exten => _00ZX.,n(noddi),NoOp(NAO Permitir)
 * ...
 * exten => _00ZX.,n,Hangup()
 */
$ramaisValidos = array (
		'4901','4902','4903','4904','4908','4909',
		'4912','4913','4922','4924','4925','4926',
		'4928','4929','4930','4931','4932','4933',
		'4934','4935','4941','4943','4944','4946',
		'4948',
	);

// Devolve o parametro para o asterisk atraves
// da variavel ${PERMITIR}, podendo conter yes ou no
$permitir = in_array($ramal, $ramaisValidos) ? 'yes' : 'no';
$agi->set_variable("PERMITIR",$permitir);

exit();

?>
