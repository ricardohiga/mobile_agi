#!/usr/bin/php
<?php

/**
 * Imports
 */
require_once ('bmconnector/config/Bootstrap.php');
require_once ('bmconnector/dao/OfflineCallDAO.php');
require_once ('bmconnector/persistence/Persistence.php');

/**
 * Instanciando os objetos de classe
 */
$offline = new OfflineCallDAO();

$argv[0] = '';
$argv[1] = 'answersainte';
$argv[2] = 'suporte';
$argv[3] = 'suporte';
$argv[4] = 'suporte';

/**
 * Data utc
 */
$timezone = new DateTimeZone('UTC');
$date = new DateTime("now", $timezone);
$utc = $date->format('Y-m-d H:i:s');

switch (strtolower($argv[1])) {
			
	case 'answersainte':
			// exten
			$offline->answerSainte($argv[2]);
		break;
		
	case 'answerentrante':
			// exten, origin, dstchannel			
			$offline->answerEntrante($argv[2],$argv[3],$argv[4]);
		break;
		
	case 'answerentrantedial':
			// origin, dstchannel			
			$offline->answerEntranteDial($argv[2]);
		break;

	default :
		$agi->noop('Metodo ' .$controller->parameter['app']. ' nao encontrado');
		break;	
		
}

echo ('finalizando');

exit ();
?>

